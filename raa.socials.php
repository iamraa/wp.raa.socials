<?php
/*
Plugin Name: RAA.Socials
Description: Add social buttons to articles
Version: 0.1
License: GPL2

*/

/**
 * Class RaaSocials
 *
 * Add social buttons to articles
 *
 * Use:
 * https://github.com/sapegin/social-likes
 * Demo:
 * http://social-likes.js.org/ru/
 */
class RaaSocials {
	public static $options = [
		'is_visible' => '0',
	];
	static $_scriptIncluded = false;
	protected static $_socialTemplate = null;

	public static function init() {
		self::getOptions();
		if ( ! is_admin() && ( isset( $_SERVER['SCRIPT_NAME'] ) && $_SERVER['SCRIPT_NAME'] != '/wp-login.php' )
		     && ! empty( self::$options['is_visible'] )
		) {
			// Add social buttons
			add_filter( 'the_content', [ 'RaaSocials', 'addSocialButtons' ] );

		}

	}

	protected static function getOptions() {
		$options = get_option( 'raa-socials' );
		if ( ! empty( $options ) ) {
			self::$options = $options;
		}

	}

	/**
	 * Add social buttons to posts in loop and single
	 *
	 * @param $content
	 *
	 * @return string
	 */
	static function addSocialButtons( $content ) {
		global $wp_query;
		global $post;

		if ( is_feed() || $wp_query->is_search ) {
			return $content;
		}
		if ( ! RaaSocials::$_socialTemplate ) {
			RaaSocials::$_socialTemplate = file_get_contents( __DIR__ . '/templates/social.html' );
		}

		if ( strpos( $content, 'sociallocker' ) !== false && ! empty( self::$options['is_hide_wlocker'] ) ) {
			// Don't add buttons at page with SocialLocker
			return $content;
		} else if ( strpos( get_permalink(), 'checkout' ) !== false
                    || strpos( get_permalink(), 'cart' ) !== false
		            || strpos( get_permalink(), 'profil/' ) !== false ) {
			// Don't add buttons at Checkout page
			return $content;
		} else if ( ! empty( $post )
		            && ( in_array( $post->post_type, array( 'product' ) )
		                 || strpos( $post->post_type, 'lp_' ) !== false ) ) {
			// Don't add buttons at Product, Course & Lesson pages
			return $content;
		}

		$content .= strtr( RaaSocials::$_socialTemplate, [ '%permalink%' => get_permalink() ] );

		// Add scripts & styles
		if ( ! self::$_scriptIncluded ) {
			add_action( 'wp_footer', [ 'RaaSocials', 'footer' ] );
			self::$_scriptIncluded = true;
		}

		return $content;
	}

	public static function footer() {
		?>
        <link rel="stylesheet" href="/wp-content/plugins/raa-socials/social-likes/social-likes_flat.css">
        <script src="/wp-content/plugins/raa-socials/social-likes/social-likes.min.js"></script>
		<?php
	}

	public static function activate() {
		add_option( 'raa-socials', self::$options );
	}

	public static function deactive() {
		delete_option( 'raa-socials' );
	}

	public static function adminInit() {
		register_setting( 'raa-socials', 'raa-socials' );

	}

	public static function adminMenu() {
		add_options_page(
			'RAA.Socials', 'RAA.Socials',
			'manage_options', 'RaaSocials',
			[ 'RaaSocials', 'optionsPage' ]
		);

	}

	public static function optionsPage() {
		self::getOptions();
		include dirname( __FILE__ ) . '/options.php';

	}

}

register_activation_hook( __FILE__, [ 'RaaSocials', 'activate' ] );
register_deactivation_hook( __FILE__, [ 'RaaSocials', 'deactive' ] );

if ( is_admin() ) {
	add_action( 'admin_init', [ 'RaaSocials', 'adminInit' ] );
	add_action( 'admin_menu', [ 'RaaSocials', 'adminMenu' ] );
} else {
	// add action
	add_action( 'init', [ 'RaaSocials', 'init' ] );
}