<div class="wrap">
    <h2>RAA: социальные кнопки</h2>

    <form method="post" action="options.php">
		<?php wp_nonce_field( 'update-options' ); ?>
		<?php settings_fields( 'raa-socials' ); ?>

        <table class="form-table">

            <tr valign="top">
                <th scope="row">Показывать кнопки:</th>
                <td><input type="checkbox" name="raa-socials[is_visible]"
                           value="1" <?= ! empty( RaaSocials::$options['is_visible'] ) ? 'checked' : ''; ?>/></td>
            </tr>
            <tr valign="top">
                <th scope="row">Скрывать кнопки при наличии Социального замка:</th>
                <td><input type="checkbox" name="raa-socials[is_hide_wlocker]"
                           value="1" <?= ! empty( RaaSocials::$options['is_hide_wlocker'] ) ? 'checked' : ''; ?>/></td>
            </tr>

        </table>

        <input type="hidden" name="action" value="update"/>

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>"/>
        </p>

    </form>
</div>
